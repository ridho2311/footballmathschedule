package dev.snowyowl.com.footballmatchschedule.presenterView

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import dev.snowyowl.com.footballmatchschedule.model.ResponseTeam
import dev.snowyowl.com.footballmatchschedule.utils.ApiAddress
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DetailPresenterImpTest{
    @Mock
    private lateinit var view: DetailView

    @Mock
    private lateinit var presenter: DetailPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = DetailPresenterImp(view)
    }

    @Test
    fun getImageTest(){
        // butuh referensi unit test buat fuel mas
        (ApiAddress.getImageLogo("Arsenal").replace(" ", "%20")).httpGet().responseObject(ResponseTeam.DeserializeTeam()) { _, response, result ->
            when (result) {
                is Result.Success -> {
                    val (value) = result
                    if (value != null) {
                        if (!value.teams.isEmpty()) {
                            Mockito.verify(view).onSuccess(value.teams[0].strTeamBadge, true)
                        }
                    } else {
                        Mockito.verify(view).onFailure(response.responseMessage)
                    }
                }
                is Result.Failure -> {
                    result.error.message?.let { Mockito.verify(view).onFailure(it) }
                }
            }
        }
        presenter.getImage("Arsenal", true)
    }
}