package dev.snowyowl.com.footballmatchschedule.presenterView

import android.util.Log
import com.github.kittinunf.fuel.core.Client
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import dev.snowyowl.com.footballmatchschedule.model.ResponseEvent
import dev.snowyowl.com.footballmatchschedule.utils.ApiAddress
import dev.snowyowl.com.footballmatchschedule.utils.TestContextProvider
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import kotlin.coroutines.experimental.coroutineContext

class SchedulePresenterImpTest {

    @Mock
    private lateinit var presenter: SchedulePresenter

    @Mock
    private lateinit var view: ScheduleView

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = SchedulePresenterImp(view, TestContextProvider())
    }

    @Test
    fun getLastSchedule() {
        // butuh referensi unit test buat fuel mas
        ApiAddress.getPrevMatch("4328").httpGet().responseObject(ResponseEvent.DeserializeSchedule()) { request, response, result ->
            when (result) {
                is Result.Success -> {
                    val (value) = result
                    if (value?.events != null) {
                        Log.d("testgetLastSchedule", "size list = ${value.events.size}")
                    }
                }
                is Result.Failure -> {
                    Log.d("testgetLastSchedule", "error  = ${result.error.message!!}")
                }
            }
        }
        presenter.getLastSchedule("4328")
    }
}