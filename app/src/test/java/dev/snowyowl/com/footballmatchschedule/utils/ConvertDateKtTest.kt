package dev.snowyowl.com.footballmatchschedule.utils

import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class ConvertDateKtTest{

    @Test
    fun dateToGmt7(){
        var dateTest = "2018-10-21 15:00:00+0000"
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale("ID"))
        val date = dateFormat.parse(dateTest)
        val dateConverted = dateFormat.format(date.time)

        assertEquals("2018-10-21 22:00:00+0700", Converter.dateToGmt7(dateTest))
    }
}