package dev.snowyowl.com.footballmatchschedule

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.widget.EditText
import dev.snowyowl.com.footballmatchschedule.ankoUi.TeamsUi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest{
    @Rule
    @JvmField
    var mActivityTestResult = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest(){
        Thread.sleep(4000)
        onView(withId(R.id.menu_search)).perform(click())
        onView(isAssignableFrom(EditText::class.java)).perform(typeText("arse"), closeSoftKeyboard())
        Thread.sleep(5000)
        onView(isAssignableFrom(EditText::class.java)).perform(typeText("n"), closeSoftKeyboard())
        Thread.sleep(5000)
        onView(withId(R.id.menu_teams)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(3000)
        onView(withId(TeamsUi.recycleView)).check(matches(isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(3000)
        onView(withId(R.id.op_faforite)).check(matches(isDisplayed())).perform(click())
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(3000)
        onView(withId(TeamsUi.spinner)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(2000)
        onView(withText("Italian Serie A")).check(matches(isDisplayed())).perform(click())
        Thread.sleep(5000)
        onView(withId(TeamsUi.recycleView)).check(matches(isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(3000)
        onView(withText(R.string.Players)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(3000)
        onView(withText("Adriano Ferreira Pinto")).check(matches(isDisplayed())).perform(click())
        Thread.sleep(5000)
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(3000)
        onView(withId(R.id.op_faforite)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(2000)
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(3000)
        onView(withId(R.id.menu_favorite)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(3000)
        onView(withText("TEAMS")).check(matches(isDisplayed())).perform(click())
        Thread.sleep(3000)
    }
}