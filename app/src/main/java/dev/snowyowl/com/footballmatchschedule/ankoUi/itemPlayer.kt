package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.text.Layout
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import org.jetbrains.anko.*

class itemPlayer : AnkoComponent<ViewGroup> {
    companion object {
        const val img = 23
        const val txName = 24
        const val txPos = 25
    }
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        linearLayout {
            lparams(width = matchParent, height = wrapContent){
                padding = 8
                orientation = LinearLayout.HORIZONTAL
            }
            imageView {
                id = img
            }.lparams(width = 90, height = 90){
                marginEnd = 8
            }
            verticalLayout {
                lparams(width = wrapContent, height = wrapContent)
                textView {
                    id = txName
                }.lparams(width = wrapContent, height = wrapContent)
                textView {
                    id = txPos
                }.lparams(width = wrapContent, height = wrapContent)
            }


        }
    }
}