package dev.snowyowl.com.footballmatchschedule.presenterView

import dev.snowyowl.com.footballmatchschedule.model.Leagues
import dev.snowyowl.com.footballmatchschedule.model.Team

interface TeamsView {
    fun onSuccessLeagues(data : ArrayList<Leagues>)
    fun onTeamLoaded(data : ArrayList<Team>)
    fun onFailure(msg : String)
    fun showLoading()
    fun hideLoading()
}