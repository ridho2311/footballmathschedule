package dev.snowyowl.com.footballmatchschedule.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.adapter.PlayerAdapter
import dev.snowyowl.com.footballmatchschedule.ankoUi.TeamInfoUi
import dev.snowyowl.com.footballmatchschedule.model.Player
import dev.snowyowl.com.footballmatchschedule.model.ResponsePlayer
import dev.snowyowl.com.footballmatchschedule.model.Team
import dev.snowyowl.com.footballmatchschedule.utils.ApiAddress
import dev.snowyowl.com.footballmatchschedule.utils.ExpressoIdlingResource
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.toast

class TeamInfoFragment : Fragment() {
    private var recyclerView : RecyclerView? = null
    private var txOverview : TextView? = null
    private var list : ArrayList<Player>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = context?.let { AnkoContext.Companion.create(it, this, false) }?.let { TeamInfoUi().createView(it) }
        initView(view)
        return view
    }

    private fun initView(view: View?) {
        list = ArrayList()

        val txDesc = view?.findViewById<TextView>(R.id.tx_overview)
        txOverview = view?.findViewById(R.id.tx_overview)
        recyclerView = view?.findViewById(R.id.rv_player)
        val dJson = arguments?.getString("data")
        val data = Gson().fromJson(dJson, Team::class.java) as Team

        if(arguments?.getString("key") == getString(R.string.Overview)){
            recyclerView?.visibility = View.GONE
            txDesc?.text = data.strDescriptionEN
        }else{
            txOverview?.visibility = View.GONE
            recyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            if(list != null)
                recyclerView?.adapter = PlayerAdapter(list!!)
            if(data.idTeam != null){
                getPlayer(data.idTeam)
            }
            else
                toast("kesalahan")
        }
    }

    private fun getPlayer(idTeam : String){
        ExpressoIdlingResource.increment()
        ApiAddress.getPlayerByTeam(idTeam).httpGet().responseObject(ResponsePlayer.DeserializePlayer()){
            request, response, result ->
            ExpressoIdlingResource.decrement()
            when (result) {
                is Result.Success -> {
                    val (value) = result
                    if (value?.player != null) {
                        if(value.player.isNotEmpty()){
                            list?.clear()
                            list?.addAll(value.player)
                            recyclerView?.adapter?.notifyDataSetChanged()
                        }
                        else
                            toast("Tidak tersedia")
                    } else {
                        toast("Tidak tersedia")
                    }
                }
                is Result.Failure -> {
                    toast(result.error.message!!)
                }
            }
        }
    }

}
