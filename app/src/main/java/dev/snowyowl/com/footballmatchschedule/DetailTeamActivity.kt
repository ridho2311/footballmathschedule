package dev.snowyowl.com.footballmatchschedule

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dev.snowyowl.com.footballmatchschedule.adapter.ViewPagerAdapter
import dev.snowyowl.com.footballmatchschedule.fragment.TeamInfoFragment
import dev.snowyowl.com.footballmatchschedule.helper.data
import dev.snowyowl.com.footballmatchschedule.helper.database
import dev.snowyowl.com.footballmatchschedule.helper.idEvent
import dev.snowyowl.com.footballmatchschedule.helper.tableNameTeam
import dev.snowyowl.com.footballmatchschedule.model.DbFavorite
import dev.snowyowl.com.footballmatchschedule.model.Team
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class DetailTeamActivity : AppCompatActivity() {
    private var isFavorite = false
    private var dataTeam : Team? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)
        initView()
    }

    private fun initView() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = ""

        val dJson = intent.getStringExtra("data")
        val data = Gson().fromJson(dJson, Team::class.java) as Team
        dataTeam = data

        val img = findViewById<ImageView>(R.id.img_badge)
        val name = findViewById<TextView>(R.id.tx_name_team)
        val year = findViewById<TextView>(R.id.tx_year)
        val stadion = findViewById<TextView>(R.id.tx_stadion)

        Picasso.get().load(data.strTeamBadge).into(img)
        name.text = data.strTeam
        year.text = data.intFormedYear
        stadion.text = data.strStadium

        val list = ArrayList<Fragment>()
        val overview = TeamInfoFragment()
        val player = TeamInfoFragment()
        val bundleOverview = Bundle()
        bundleOverview.putString("key", getString(R.string.Overview))
        bundleOverview.putString("title", getString(R.string.Overview))
        bundleOverview.putString("data", dJson)
        overview.arguments = bundleOverview
        val bundlePlayer = Bundle()
        bundlePlayer.putString("key", getString(R.string.Players))
        bundlePlayer.putString("title", getString(R.string.Players))
        bundlePlayer.putString("data", dJson)
        player.arguments = bundlePlayer
        list.add(overview)
        list.add(player)
        // set adapter
        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        val tabLayout = findViewById<TabLayout>(R.id.tablayout)
        val pagerAdapter = ViewPagerAdapter(list, supportFragmentManager)

        viewPager.adapter = pagerAdapter
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.backgroundColor = ContextCompat.getColor(baseContext, R.color.colorPrimary)
        tabLayout.setTabTextColors(ContextCompat.getColor(baseContext, R.color.white), Color.WHITE)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val id = dataTeam?.idTeam
        val listS = baseContext.database.use {
            select(tableNameTeam, idEvent, data).whereArgs("$idEvent = {id}", "id" to "$id")
                    .parseList(object : MapRowParser<ArrayList<DbFavorite>> {
                override fun parseRow(columns: Map<String, Any?>): ArrayList<DbFavorite> {
                    val list = ArrayList<DbFavorite>()
                    val id: String = columns[dev.snowyowl.com.footballmatchschedule.helper.idEvent].toString()
                    val data: String = columns[dev.snowyowl.com.footballmatchschedule.helper.data].toString()
                    list.add(DbFavorite(id, data))

                    return list
                }
            })
        }
        isFavorite = listS.isNotEmpty()
        if (isFavorite)
            menuInflater.inflate(R.menu.favorite_menu_2, menu)
        else
            menuInflater.inflate(R.menu.favorite_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.op_faforite -> {
                if (isFavorite) {
                    item.icon = ContextCompat.getDrawable(baseContext, R.drawable.ic_star_border_white_24dp)
                    val id = dataTeam?.idTeam
                    val a = baseContext.database.use {
                        delete(tableNameTeam, whereClause = "${dev.snowyowl.com.footballmatchschedule.helper.idEvent} = {id}", args = "id" to "$id")
                    }
                    toast("berhasil dihapus")
                } else {
                    item.icon = ContextCompat.getDrawable(baseContext, R.drawable.ic_star_white_24dp)
                    val id = dataTeam?.idTeam
                    val dataS = Gson().toJson(dataTeam)
                    if (id != null) {
                         val b = baseContext.database.use {
                            insert(tableNameTeam, dev.snowyowl.com.footballmatchschedule.helper.idEvent to id,
                                    dev.snowyowl.com.footballmatchschedule.helper.data to dataS)
                        }
                        toast("berhasil disimpan")
                    }

                }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun getSupportParentActivityIntent(): Intent? {
        onBackPressed()
        return null
    }

}
