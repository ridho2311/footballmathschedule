package dev.snowyowl.com.footballmatchschedule.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.Spinner
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.adapter.SpinnerAdapter
import dev.snowyowl.com.footballmatchschedule.adapter.TeamAdapter
import dev.snowyowl.com.footballmatchschedule.ankoUi.ScheduleUi
import dev.snowyowl.com.footballmatchschedule.ankoUi.TeamsUi
import dev.snowyowl.com.footballmatchschedule.model.Leagues
import dev.snowyowl.com.footballmatchschedule.model.Team
import dev.snowyowl.com.footballmatchschedule.presenterView.TeamsPresenter
import dev.snowyowl.com.footballmatchschedule.presenterView.TeamsPresenterImp
import dev.snowyowl.com.footballmatchschedule.presenterView.TeamsView
import dev.snowyowl.com.footballmatchschedule.utils.CoroutineContextProvider
import dev.snowyowl.com.footballmatchschedule.utils.ExpressoIdlingResource
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.toast


class TeamsFragment : Fragment(), TeamsView {
    private var presenter: TeamsPresenter? = null
    private var recyclerView: RecyclerView? = null
    private var rvAdapter : TeamAdapter? = null
    private var spinner: Spinner? = null
    private var spinnerAdapter : SpinnerAdapter? =null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var listLeagueComplete: ArrayList<Leagues>? = null
    private var list : ArrayList<Team>? = null
    private var idLeagues = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        if(arguments?.getString("key") == getString(R.string.favorites)){
            context?.let {
                ExpressoIdlingResource.increment()
                presenter?.getFavoriteTeam(it)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = context?.let { AnkoContext.Companion.create(it, this, false) }?.let { TeamsUi().createView(it) }
        initView(view)
        ExpressoIdlingResource.increment()
        if(arguments?.getString("key") == getString(R.string.favorites)){
            context?.let {
                ExpressoIdlingResource.increment()
                presenter?.getFavoriteTeam(it)
            }
        }else{
            ExpressoIdlingResource.increment()
            presenter?.getListLeagues()
        }
        return view
    }

    override fun onTeamLoaded(data: ArrayList<Team>) {
        list?.clear()
        recyclerView?.adapter?.notifyDataSetChanged()
        list?.addAll(data)
        recyclerView?.adapter?.notifyDataSetChanged()
        ExpressoIdlingResource.decrement()
    }

    override fun onSuccessLeagues(data: ArrayList<Leagues>) {
        if(data.isNotEmpty() && listLeagueComplete != null){
            listLeagueComplete?.addAll(data)
            spinnerAdapter?.notifyDataSetChanged()
        }
        ExpressoIdlingResource.decrement()
    }

    override fun onFailure(msg: String) {
        list?.clear()
        recyclerView?.adapter?.notifyDataSetChanged()
        toast(msg)
        ExpressoIdlingResource.decrement()
    }

    override fun showLoading() {
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
    }

    private fun initView(view: View?) {
        presenter = TeamsPresenterImp(this, CoroutineContextProvider())
        list = ArrayList()
        recyclerView = view?.findViewById(TeamsUi.recycleView)
        recyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        if(list != null){
            rvAdapter = TeamAdapter(list!!)
            recyclerView?.adapter = rvAdapter
        }

        swipeRefreshLayout = view?.findViewById(R.id.swipe)
        spinner = view?.findViewById(ScheduleUi.spinner)

        if(arguments?.getString("key") != getString(R.string.favorites)) {
            listLeagueComplete = ArrayList()
            spinnerAdapter = SpinnerAdapter(listLeagueComplete!!)
            spinner?.adapter = spinnerAdapter
            spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    if(listLeagueComplete != null){
                        idLeagues = listLeagueComplete?.get(0)?.idLeague.toString()
                        spinner?.setSelection(0)
                    }else{
                        toast("Tidak tersedia")
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if(listLeagueComplete?.isNotEmpty()!!){
                        idLeagues = listLeagueComplete?.get(position)?.strLeague.toString()
                        ExpressoIdlingResource.increment()
                        presenter?.getTeams(idLeagues)
                    }

                }
            }
        }else{
            spinner?.visibility = View.GONE
        }

        swipeRefreshLayout?.setOnRefreshListener {
            ExpressoIdlingResource.increment()
            if(arguments?.getString("key") == getString(R.string.favorites))
                context?.let { presenter?.getFavoriteTeam(it) }
            else
                presenter?.getTeams(idLeagues)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        if(arguments?.getString("key") != getString(R.string.favorites)){
            inflater?.inflate(R.menu.option_menu, menu)
            val searchItem = menu?.findItem(R.id.menu_search)
            val searchView = searchItem?.actionView as SearchView
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    rvAdapter?.filter?.filter(p0)
                    return true
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    rvAdapter?.filter?.filter(p0)
                    return true
                }

            })
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

}
