package dev.snowyowl.com.footballmatchschedule.utils

import java.text.SimpleDateFormat
import java.util.*

class Converter {
    companion object {
        fun dateToGmt7(time: String) : String{
            val simpleTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale("ID"))
            val date = simpleTimeFormat.parse(time)
            return simpleTimeFormat.format(date.time)
        }

        fun getDate(date: String) : String{
            val simpleTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale("ID"))
            val simpleTimeFormatDateOnly = SimpleDateFormat("EEEE, dd MMM yyyy", Locale("ID"))
            val dateConvert = simpleTimeFormat.parse(dateToGmt7(date))

            return simpleTimeFormatDateOnly.format(dateConvert)
        }

        fun getHours(date: String) : String{
            val simpleTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale("ID"))
            val simpleTimeFormatDateOnly = SimpleDateFormat("HH:mm", Locale("ID"))
            val dateConvert = simpleTimeFormat.parse(dateToGmt7(date))

            return simpleTimeFormatDateOnly.format(dateConvert) + " WIB"
        }
    }
}