package dev.snowyowl.com.footballmatchschedule.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

class ResponsePlayer(val player : ArrayList<Player>) {
    class DeserializePlayer : ResponseDeserializable<ResponsePlayer>{
        override fun deserialize(content: String): ResponsePlayer? = Gson().fromJson(content, ResponsePlayer::class.java)
    }
}