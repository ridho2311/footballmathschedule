package dev.snowyowl.com.footballmatchschedule.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

const val tableNameSchedule = "favoriteTable"
const val tableNameTeam = "teamsTable"
const val idEvent = "idEvent"
const val data = "data"

class DatabaseHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, tableNameSchedule, null, 2) {
    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(tableNameSchedule, true, idEvent to TEXT + PRIMARY_KEY, data to TEXT)
        db?.createTable(tableNameTeam, true, idEvent to TEXT + PRIMARY_KEY, data to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(tableNameSchedule, true)
        db?.dropTable(tableNameTeam, true)
        onCreate(db)
    }

}

val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)
