package dev.snowyowl.com.footballmatchschedule.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

class ResponseTeam(val teams : ArrayList<Team>) {
    class DeserializeTeam : ResponseDeserializable<ResponseTeam>{
        override fun deserialize(content: String): ResponseTeam? = Gson().fromJson(content, ResponseTeam::class.java)
    }
}