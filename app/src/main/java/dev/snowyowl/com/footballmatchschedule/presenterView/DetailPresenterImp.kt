package dev.snowyowl.com.footballmatchschedule.presenterView

import android.content.Context
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import dev.snowyowl.com.footballmatchschedule.helper.data
import dev.snowyowl.com.footballmatchschedule.helper.database
import dev.snowyowl.com.footballmatchschedule.helper.tableNameSchedule
import dev.snowyowl.com.footballmatchschedule.model.DbFavorite
import dev.snowyowl.com.footballmatchschedule.model.ResponseTeam
import dev.snowyowl.com.footballmatchschedule.utils.ApiAddress
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailPresenterImp(val view: DetailView) : DetailPresenter {
    override fun getFavoriteDb(idEvent: String, context : Context) {
        val listS = context.database.use {
            select(tableNameSchedule, dev.snowyowl.com.footballmatchschedule.helper.idEvent, data).whereArgs("${dev.snowyowl.com.footballmatchschedule.helper.idEvent} = {id}", "id" to idEvent).parseList(object : MapRowParser<ArrayList<DbFavorite>> {
                override fun parseRow(columns: Map<String, Any?>): ArrayList<DbFavorite> {
                    val list = ArrayList<DbFavorite>()
                    val id: String = columns[dev.snowyowl.com.footballmatchschedule.helper.idEvent].toString()
                    val data: String = columns[data].toString()
                    list.add(DbFavorite(id, data))

                    return list
                }
            })
        }
        view.hasFavorite(listS.isNotEmpty())
    }

    override fun getImage(nameTeam: String, isHome: Boolean) {
        (ApiAddress.getImageLogo(nameTeam).replace(" ", "%20")).httpGet().responseObject(ResponseTeam.DeserializeTeam()) { _, response, result ->
            when (result) {
                is Result.Success -> {
                    val (value) = result
                    if (value != null) {
                        if (!value.teams.isEmpty()) {
                            view.onSuccess(value.teams[0].strTeamBadge, isHome)
                        }
                    } else {
                        view.onFailure(response.responseMessage)
                    }
                }
                is Result.Failure -> {
                    result.error.message?.let { view.onFailure(it) }
                }
            }
        }
    }

    override fun insertFavorite(idEvent: String, data : String, context: Context){
        context.database.use {
            insert(tableNameSchedule, dev.snowyowl.com.footballmatchschedule.helper.idEvent to idEvent,
                    dev.snowyowl.com.footballmatchschedule.helper.data to data)
        }
        view.successInsertFavorite()
    }

    override fun deleteFavorite(idEvent: String, context: Context){
        context.database.use {
            delete(tableNameSchedule, whereClause = "${dev.snowyowl.com.footballmatchschedule.helper.idEvent} = {id}", args = "id" to idEvent)
        }
        view.successDeleteFavorite()
    }
}