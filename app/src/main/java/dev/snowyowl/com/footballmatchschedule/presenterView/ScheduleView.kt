package dev.snowyowl.com.footballmatchschedule.presenterView

import dev.snowyowl.com.footballmatchschedule.model.EventSchedule
import dev.snowyowl.com.footballmatchschedule.model.Leagues

interface ScheduleView {
    fun onSuccessSchedule(data : ArrayList<EventSchedule>)
    fun onSuccessLeagues(data : ArrayList<Leagues>)
    fun onFailure(msg : String)
    fun onFailureNoSchedule(msg : String)
    fun showLoading()
    fun hideLoading()
}