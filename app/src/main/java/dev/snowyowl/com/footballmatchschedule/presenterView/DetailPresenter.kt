package dev.snowyowl.com.footballmatchschedule.presenterView

import android.content.Context

interface DetailPresenter {
    fun getFavoriteDb(idEvent: String, context : Context)
    fun getImage(nameTeam: String, isHome: Boolean)
    fun insertFavorite(idEvent: String, data : String, context: Context)
    fun deleteFavorite(idEvent: String, context: Context)
}