package dev.snowyowl.com.footballmatchschedule.model

class Team(val idTeam : String,
           val strTeam : String,
           val strDescriptionEN : String,
           val strTeamBadge : String,
           val intFormedYear : String,
           val strStadium : String)