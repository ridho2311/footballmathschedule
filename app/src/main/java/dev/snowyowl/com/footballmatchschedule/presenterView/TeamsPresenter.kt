package dev.snowyowl.com.footballmatchschedule.presenterView

import android.content.Context

interface TeamsPresenter {
    fun getListLeagues()
    fun getFavoriteTeam(context: Context)
    fun getTeams(idLeagues : String)
}