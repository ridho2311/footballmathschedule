package dev.snowyowl.com.footballmatchschedule.utils

import dev.snowyowl.com.footballmatchschedule.BuildConfig

class ApiAddress {
    companion object {
        private const val prefMatch = "eventspastleague.php?id="
        private const val nextMatch = "eventsnextleague.php?id="
        private const val imageTeamLogo = "searchteams.php?t="
        private const val listLeague = "all_leagues.php"
        private const val allTeam = "search_all_teams.php?l="
        private const val player = "lookup_all_players.php?id="

        fun getNextMatch(idLeagues : String) : String = BuildConfig.Base_url+ nextMatch + idLeagues
        fun getPrevMatch(idLeagues: String) : String = BuildConfig.Base_url+ prefMatch + idLeagues
        fun getImageLogo(idTeam : String) : String = BuildConfig.Base_url + imageTeamLogo + idTeam
        fun getAllLeague() : String = BuildConfig.Base_url + listLeague
        fun getAllTeamByLeagues(leagues : String) : String = BuildConfig.Base_url + allTeam + leagues
        fun getPlayerByTeam(teamId : String) : String = BuildConfig.Base_url + player + teamId
    }
}