package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.view.View
import dev.snowyowl.com.footballmatchschedule.fragment.ContMatchFragment
import dev.snowyowl.com.footballmatchschedule.R
import org.jetbrains.anko.*
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.viewPager

class MatchUi : AnkoComponent<ContMatchFragment> {
    override fun createView(ui: AnkoContext<ContMatchFragment>): View = with(ui){
        verticalLayout {
            tabLayout {
                id = R.id.tablayout
            }.lparams(width = matchParent)
            viewPager {
                id = R.id.viewpager
            }.lparams(width = matchParent, height = matchParent)
        }
    }
}