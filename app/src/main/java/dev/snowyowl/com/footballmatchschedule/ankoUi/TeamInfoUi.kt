package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.view.View
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.fragment.TeamInfoFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.nestedScrollView

class TeamInfoUi : AnkoComponent<TeamInfoFragment> {
    override fun createView(ui: AnkoContext<TeamInfoFragment>): View = with(ui){
        verticalLayout {
            nestedScrollView {
                textView {
                    id = R.id.tx_overview
                }.lparams(width = matchParent, height = wrapContent){
                    padding = 16
                }
            }
            recyclerView {
                id = R.id.rv_player
            }.lparams(width = matchParent, height = wrapContent)
        }
    }
}