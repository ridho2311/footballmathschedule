package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.LinearLayout
import dev.snowyowl.com.footballmatchschedule.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class ItemScheduleUi : AnkoComponent<ViewGroup> {
    companion object {
        const val txTime = 4
        const val txDate = 5
        const val txLeftTim = 6
        const val txLeftTimScore = 7
        const val txRightTim = 8
        const val txRightTimScore = 9
        const val icNotif = 10
    }
    override fun createView(ui: AnkoContext<ViewGroup>): View  = with(ui){
        cardView {
            lparams {
                width = matchParent
                height = wrapContent
                margin = dip(4)
                radius = 6f
                cardElevation = 3f
                padding = 4
                verticalLayout {
                    lparams(width = matchParent, height = wrapContent){
                        padding = 8
                    }
                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)
                        textView {
                            id = txDate
                            textSize = 12F
                            textColor = R.color.colorPrimary
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams {
                            width = matchParent
                            bottomMargin = dip(8)
                            gravity = Gravity.CENTER_HORIZONTAL
                        }
                        imageView {
                            id = icNotif
                            imageResource = R.drawable.ic_notifications_active_black_24dp
                        }.lparams(width = 50, height = 50){
                            margin = 4
                            alignParentEnd()
                        }
                    }
                    textView {
                        id = txTime
                        textSize = 12F
                        textColor = R.color.colorPrimary
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams {
                        width = matchParent
                        bottomMargin = dip(8)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        lparams(width = wrapContent, height = wrapContent){
                            gravity = Gravity.CENTER_HORIZONTAL
                        }
                        gravity = Gravity.CENTER_HORIZONTAL
                        textView {
                            id = txLeftTim
                            text = "_"
                            textSize = 20F
                        }.lparams {
                            rightMargin = 10
                        }
                        textView {
                            id = txLeftTimScore
                            textSize = 18F
                            typeface = Typeface.DEFAULT_BOLD
                            text = "0"
                            textSize = 22F
                        }.lparams {
                            rightMargin = 10
                        }
                        textView { text = "vs" }
                        textView {
                            id = txRightTimScore
                            typeface = Typeface.DEFAULT_BOLD
                            textSize = 18F
                            text = "0"
                            textSize = 22F
                        }.lparams {
                            leftMargin = 10
                        }
                        textView {
                            id = txRightTim
                            text = "_"
                            textSize = 20F
                        }.lparams {
                            leftMargin = 10
                            bottomMargin = 8
                        }

                    }
                }
            }
        }
    }
}