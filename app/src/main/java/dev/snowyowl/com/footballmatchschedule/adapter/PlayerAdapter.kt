package dev.snowyowl.com.footballmatchschedule.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dev.snowyowl.com.footballmatchschedule.DetailScheduleActivity
import dev.snowyowl.com.footballmatchschedule.DetailTeamActivity
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.ankoUi.ItemTeam
import dev.snowyowl.com.footballmatchschedule.ankoUi.itemPlayer
import dev.snowyowl.com.footballmatchschedule.model.Player
import dev.snowyowl.com.footballmatchschedule.model.Team
import dev.snowyowl.com.footballmatchschedule.utils.DetailPlayerActivity
import org.jetbrains.anko.AnkoContext

class PlayerAdapter(private val list : ArrayList<Player>) : RecyclerView.Adapter<PlayerAdapter.viewHolderSchedule>(){
    private var context : Context? = null
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): viewHolderSchedule {
        this.context = p0.context
        return viewHolderSchedule(itemPlayer().createView(AnkoContext.create(p0.context, p0)))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: viewHolderSchedule, p1: Int) {
       p0.onBind(list[p1])
        p0.itemView.setOnClickListener {
            context?.startActivity(Intent(context, DetailPlayerActivity::class.java)
                    .putExtra("data", Gson().toJson(list[p1])))
        }
    }

    class viewHolderSchedule(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(data : Player){
            val img = itemView.findViewById<ImageView>(itemPlayer.img)
            val name = itemView.findViewById<TextView>(itemPlayer.txName)
            val pos = itemView.findViewById<TextView>(itemPlayer.txPos)
            Picasso.get().load(data.strCutout).into(img)
            name.text = data.strPlayer
            pos.text = data.strPosition

        }
    }
}