package dev.snowyowl.com.footballmatchschedule.presenterView

import android.content.Context

interface SchedulePresenter {
    fun getNextSchedule(idLeagues : String)
    fun getLastSchedule(idLeagues: String)
    fun getFavorite(context: Context)
    fun getListLeagues()
}