package dev.snowyowl.com.footballmatchschedule.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dev.snowyowl.com.footballmatchschedule.DetailScheduleActivity
import dev.snowyowl.com.footballmatchschedule.DetailTeamActivity
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.ankoUi.ItemTeam
import dev.snowyowl.com.footballmatchschedule.model.Team
import org.jetbrains.anko.AnkoContext

class TeamAdapter(private val list : ArrayList<Team>) : RecyclerView.Adapter<TeamAdapter.viewHolderSchedule>(), Filterable{
    private var listData : ArrayList<Team>? = null
    private var context : Context? = null

    init {
        listData = list
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val listFiltered = ArrayList<Team>()
                val strFilter = constraint?.toString()
                if(strFilter != null){
                    if(strFilter == ""){
                        listFiltered.addAll(list)
                    }else{
                        for (data in list){
                            if(data.strTeam.toLowerCase().contains(strFilter) || data.strStadium.toLowerCase().contains(strFilter)){
                                listFiltered.add(data)
                            }
                        }
                    }
                }
                val result = FilterResults()
                result.values = listFiltered
                return result
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                listData = results?.values as ArrayList<Team>
                notifyDataSetChanged()
            }

        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): viewHolderSchedule {
        this.context = p0.context
        return viewHolderSchedule(ItemTeam().createView(AnkoContext.create(p0.context, p0)))
    }

    override fun getItemCount(): Int = listData?.size!!

    override fun onBindViewHolder(p0: viewHolderSchedule, p1: Int) {
        listData?.get(p1)?.let { p0.onBind(it) }
        p0.itemView.setOnClickListener {
            context?.startActivity(Intent(context, DetailTeamActivity::class.java)
                    .putExtra("data", Gson().toJson(listData?.get(p1))))
        }
    }

    class viewHolderSchedule(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(data : Team){
            val img = itemView.findViewById<ImageView>(R.id.img_badge)
            val name = itemView.findViewById<TextView>(R.id.tx_team_name)
            Picasso.get().load(data.strTeamBadge).into(img)
            name.text = data.strTeam
        }
    }
}