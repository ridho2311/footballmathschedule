package dev.snowyowl.com.footballmatchschedule

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import dev.snowyowl.com.footballmatchschedule.ankoUi.MainActivityUi
import dev.snowyowl.com.footballmatchschedule.fragment.ContFavoriteFragment
import dev.snowyowl.com.footballmatchschedule.fragment.ContMatchFragment
import dev.snowyowl.com.footballmatchschedule.fragment.TeamsFragment
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.setContentView

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var navBottom: BottomNavigationView
    private var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUi().setContentView(this)
        intView()
        navBottom.selectedItemId = R.id.menu_mathes
    }

    private fun intView() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.backgroundColor = ContextCompat.getColor(this, R.color.colorPrimary)
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)
        navBottom = findViewById(R.id.bottom_nav)
        navBottom.inflateMenu(R.menu.nav_menu)
        navBottom.setOnNavigationItemSelectedListener(this)
        navBottom.backgroundColor = Color.WHITE
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.menu_mathes -> {
                val bundle = Bundle()
                bundle.putString("key", getString(R.string.last_match))
                fragment = ContMatchFragment()
                fragment?.arguments = bundle
            }
            R.id.menu_teams -> {
                val bundle = Bundle()
                bundle.putString("key", getString(R.string.teams))
                fragment = TeamsFragment()
                fragment?.arguments = bundle
            }
            R.id.menu_favorite -> {
                val bundle = Bundle()
                bundle.putString("key", getString(R.string.favorites))
                fragment = ContFavoriteFragment()
                fragment?.arguments = bundle
            }
        }
        if (fragment != null){
            supportFragmentManager.beginTransaction().replace(R.id.frame, fragment!!).commit()
        }
        return true
    }
}
