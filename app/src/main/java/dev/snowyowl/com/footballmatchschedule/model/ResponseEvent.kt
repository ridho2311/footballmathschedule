package dev.snowyowl.com.footballmatchschedule.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

class ResponseEvent(val events : ArrayList<EventSchedule>) {
    class DeserializeSchedule : ResponseDeserializable<ResponseEvent>{
        override fun deserialize(content: String): ResponseEvent? = Gson().fromJson(content, ResponseEvent::class.java)
    }
}