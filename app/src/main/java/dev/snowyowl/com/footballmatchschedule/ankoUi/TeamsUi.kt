package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.view.View
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.fragment.TeamsFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class TeamsUi : AnkoComponent<TeamsFragment> {
    companion object {
        const val spinner = 3
        const val recycleView = 4
    }
    override fun createView(ui: AnkoContext<TeamsFragment>): View = with(ui){
        verticalLayout {
            lparams(width  = matchParent, height = matchParent)
            spinner {
                id = spinner
            }.lparams(width = matchParent, height = wrapContent){
                topMargin = 16
                bottomMargin = 16
                leftMargin = 8
                rightMargin = 8
            }
            swipeRefreshLayout {
                id = R.id.swipe
                verticalLayout {
                    recyclerView { id = recycleView }.lparams(width = matchParent, height = wrapContent){
                        topMargin = 16
                    }
                }
            }
        }
    }
}