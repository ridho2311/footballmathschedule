package dev.snowyowl.com.footballmatchschedule.presenterView

import android.content.Context
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import dev.snowyowl.com.footballmatchschedule.helper.data
import dev.snowyowl.com.footballmatchschedule.helper.database
import dev.snowyowl.com.footballmatchschedule.helper.idEvent
import dev.snowyowl.com.footballmatchschedule.helper.tableNameSchedule
import dev.snowyowl.com.footballmatchschedule.model.DbFavorite
import dev.snowyowl.com.footballmatchschedule.model.EventSchedule
import dev.snowyowl.com.footballmatchschedule.model.ResponseEvent
import dev.snowyowl.com.footballmatchschedule.model.ResponseLeagues
import dev.snowyowl.com.footballmatchschedule.utils.ApiAddress
import dev.snowyowl.com.footballmatchschedule.utils.CoroutineContextProvider
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.select

class SchedulePresenterImp(val view: ScheduleView, private val contextProvider: CoroutineContextProvider) : SchedulePresenter {

    override fun getListLeagues() {
        async(contextProvider.main){
            view.showLoading()
            ApiAddress.getAllLeague().httpGet().responseObject(ResponseLeagues.DeserializeLeagues()){
                _, _, result ->
                view.hideLoading()
                when (result) {
                    is Result.Success -> {
                        val (value) = result
                        if (value != null) {
                            if(value.leagues.isNotEmpty())
                                view.onSuccessLeagues(value.leagues)
                            else
                                view.onFailureNoSchedule("Tidak tersedia")
                        } else {
                            view.onFailureNoSchedule("Tidak tersedia")
                        }
                    }
                    is Result.Failure -> {
                        view.onFailure(result.error.message!!)
                    }
                }
            }
        }
    }

    override fun getNextSchedule(idLeagues: String) {
        async(contextProvider.main) {
            view.showLoading()
            ApiAddress.getNextMatch(idLeagues).httpGet().responseObject(ResponseEvent.DeserializeSchedule()) { request, response, result ->
                view.hideLoading()
                when (result) {
                    is Result.Success -> {
                        val (value) = result
                        if (value != null) {
                            view.onSuccessSchedule(value.events)
                        } else {
                            view.onFailureNoSchedule("Tidak tersedia")
                        }
                    }
                    is Result.Failure -> {
                        view.onFailure(result.error.message!!)
                    }
                }
            }
        }
    }

    override fun getLastSchedule(idLeagues: String) {
        async(contextProvider.main) {
            view.showLoading()
            ApiAddress.getPrevMatch(idLeagues).httpGet().responseObject(ResponseEvent.DeserializeSchedule()) { request, response, result ->
                view.hideLoading()
                when (result) {
                    is Result.Success -> {
                        val (value) = result
                        if (value?.events != null) {
                            if(value.events.isNotEmpty())
                                view.onSuccessSchedule(value.events)
                            else
                                view.onFailureNoSchedule("Tidak tersedia")
                        } else {
                            view.onFailureNoSchedule("Tidak tersedia")
                        }
                    }
                    is Result.Failure -> {
                        view.onFailure(result.error.message!!)
                    }
                }
            }
        }

    }

    override fun getFavorite(context: Context) {
        view.showLoading()
        val l = context.database.use {
            select(tableNameSchedule, idEvent, data).parseList(object : MapRowParser<ArrayList<DbFavorite>> {
                override fun parseRow(columns: Map<String, Any?>): ArrayList<DbFavorite> {
                    val list = ArrayList<DbFavorite>()
                    val id: String = columns[idEvent].toString()
                    val data: String = columns[data].toString()
                    list.add(DbFavorite(id, data))
                    return list
                }
            })
        }
        view.hideLoading()
        if (l.isNotEmpty()) {
            val favoriteList = ArrayList<EventSchedule>()
            for ((value) in l) {
                favoriteList.add(Gson().fromJson(value.data, EventSchedule::class.java))
            }
            view.onSuccessSchedule(favoriteList)
        }
    }

}