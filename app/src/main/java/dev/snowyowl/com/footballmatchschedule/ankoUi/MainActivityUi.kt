package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.support.v4.content.res.ResourcesCompat
import android.view.View
import android.widget.LinearLayout
import dev.snowyowl.com.footballmatchschedule.MainActivity
import dev.snowyowl.com.footballmatchschedule.R
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.bottomNavigationView

class MainActivityUi : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent){
                orientation = LinearLayout.VERTICAL
            }
            toolbar {
                id = R.id.toolbar
                lparams{
                    width = matchParent
                    height = wrapContent
                }
            }
            linearLayout {
                frameLayout {
                    id = R.id.frame
                }.lparams {
                    width = matchParent
                    height = matchParent
                }
            }.lparams(width = matchParent, height = 0, weight = 1F)

            view {
                background = ResourcesCompat.getDrawable(resources, R.drawable.shadow, null)
            }.lparams {
                width = matchParent
                height = 4
                backgroundDrawable = ResourcesCompat.getDrawable(resources, R.drawable.shadow, null)
            }
            bottomNavigationView {
                id = R.id.bottom_nav
                lparams {
                    width = matchParent
                    height = wrapContent
                }
            }
        }
    }
}