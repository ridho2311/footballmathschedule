package dev.snowyowl.com.footballmatchschedule.fragment


import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.adapter.ViewPagerAdapter
import dev.snowyowl.com.footballmatchschedule.ankoUi.MatchUi
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor

class ContMatchFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = MatchUi().createView(AnkoContext.Companion.create(this.context!!, this, false))
        initView(view)
        return view
    }

    private fun initView(view: View) {
        val list = ArrayList<Fragment>()
        val scheduleFragmentLastMatch = ScheduleFragment()
        val scheduleFragmentNextMatch = ScheduleFragment()
        val bundleLast = Bundle()
        bundleLast.putString("key", getString(R.string.last_match))
        bundleLast.putString("title", getString(R.string.last_match))
        scheduleFragmentLastMatch.arguments = bundleLast
        val bundleNext = Bundle()
        bundleNext.putString("key", getString(R.string.next_match))
        bundleNext.putString("title", getString(R.string.next_match))
        scheduleFragmentNextMatch.arguments = bundleNext
        list.add(scheduleFragmentLastMatch)
        list.add(scheduleFragmentNextMatch)
        // set adapter
        val viewPager = view.findViewById<ViewPager>(R.id.viewpager)
        val tabLayout = view.findViewById<TabLayout>(R.id.tablayout)
        val pagerAdapter = ViewPagerAdapter(list, fragmentManager)

        viewPager.adapter = pagerAdapter
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.backgroundColor = ContextCompat.getColor(context!!, R.color.colorPrimary)
        tabLayout.setTabTextColors(ContextCompat.getColor(context!!, R.color.white), Color.WHITE)
    }
}
