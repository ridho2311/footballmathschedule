package dev.snowyowl.com.footballmatchschedule.model

class Player(val idPlayer : String,
             val strPlayer : String,
             val strTeam : String,
             val strPosition : String,
             val strHeight : String,
             val strWeight : String,
             val strThumb : String,
             val strCutout : String,
             val strDescriptionEN : String){

}