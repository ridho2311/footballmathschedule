package dev.snowyowl.com.footballmatchschedule

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dev.snowyowl.com.footballmatchschedule.model.EventSchedule
import dev.snowyowl.com.footballmatchschedule.presenterView.DetailPresenter
import dev.snowyowl.com.footballmatchschedule.presenterView.DetailPresenterImp
import dev.snowyowl.com.footballmatchschedule.presenterView.DetailView
import dev.snowyowl.com.footballmatchschedule.utils.ExpressoIdlingResource
import kotlinx.android.synthetic.main.activity_detail_schedule.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.toast

class DetailScheduleActivity : AppCompatActivity(), DetailView {
    private var presenterImp: DetailPresenter? = null
    private var dataSchedule: EventSchedule? = null
    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_schedule)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenterImp = DetailPresenterImp(this)
        val intentData = intent.getStringExtra("data")
        if (intentData != "") {
            dataSchedule = Gson().fromJson(intentData, EventSchedule::class.java)
            val id = dataSchedule?.idEvent
            if (id != null) {
                ExpressoIdlingResource.increment()
                presenterImp?.getFavoriteDb(id, baseContext)
            }
            if (dataSchedule != null)
                setView(dataSchedule)
        } else {
            toast("Kesalahan")
            finish()
        }
    }

    override fun hasFavorite(bool: Boolean) {
        isFavorite = bool
        ExpressoIdlingResource.increment()
    }

    override fun onSuccess(imgUrl: String, isHome: Boolean) {
        if (isHome) {
            Picasso.get().load(imgUrl).into(imgHome)
        } else {
            Picasso.get().load(imgUrl).into(imgAway)
        }
        ExpressoIdlingResource.decrement()
    }

    override fun onFailure(msg: String) {
        contentView?.let {
            snackbar(it, msg)
            ExpressoIdlingResource.decrement()
        }
    }

    override fun successInsertFavorite() {
        snackbar(contentView!!, "Ditambah ke favorite").show()
        isFavorite = true
        ExpressoIdlingResource.decrement()
    }

    override fun successDeleteFavorite() {
        snackbar(contentView!!, "Dihapus dari favorite").show()
        isFavorite = false
        ExpressoIdlingResource.decrement()
    }

    private fun setView(data: EventSchedule?) {
        txDate.text = data?.dateEvent
        tx_goal_keeper_home.text = data?.strHomeLineupGoalkeeper
        tx_goal_keeper_away.text = data?.strAwayLineupGoalkeeper
        tx_goal_home.text = data?.intHomeScore
        tx_goal_away.text = data?.intAwayScore
        txNameHome.text = data?.strHomeTeam
        txNameAway.text = data?.strAwayTeam
        tx_shot_home.text = data?.intHomeShots
        tx_shot_away.text = data?.intAwayShots
        tx_defense_home.text = data?.strHomeLineupDefense
        tx_defense_away.text = data?.strAwayLineupDefense
        tx_midfield_home.text = data?.strHomeLineupMidfield
        tx_midfield_away.text = data?.strAwayLineupMidfield
        tx_forward_home.text = data?.strHomeLineupForward
        tx_forward_away.text = data?.strAwayLineupForward
        tx_Substitute_home.text = data?.strHomeLineupSubstitutes
        tx_Substitute_away.text = data?.strAwayLineupSubstitutes
        ExpressoIdlingResource.increment()
        if (data?.strHomeTeam != null)
            presenterImp?.getImage(data.strHomeTeam, true)
        ExpressoIdlingResource.increment()
        if(data?.strAwayTeam != null)
            presenterImp?.getImage(data.strAwayTeam, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (isFavorite)
            menuInflater.inflate(R.menu.favorite_menu_2, menu)
        else
            menuInflater.inflate(R.menu.favorite_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.op_faforite -> {
                if (isFavorite) {
                    item.icon = ContextCompat.getDrawable(baseContext, R.drawable.ic_star_border_white_24dp)
                    dataSchedule?.idEvent?.let {
                        ExpressoIdlingResource.increment()
                        presenterImp?.deleteFavorite(it, baseContext)
                    }
                } else {
                    item.icon = ContextCompat.getDrawable(baseContext, R.drawable.ic_star_white_24dp)
                    val id = dataSchedule?.idEvent
                    val dataS = Gson().toJson(dataSchedule)
                    if (id != null) {
                        ExpressoIdlingResource.increment()
                        presenterImp?.insertFavorite(id, dataS, baseContext)
                    }
                }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun getSupportParentActivityIntent(): Intent? {
        onBackPressed()
        return null
    }
}
