package dev.snowyowl.com.footballmatchschedule.presenterView

interface DetailView {
    fun hasFavorite(bool : Boolean)
    fun onSuccess(imgUrl : String, isHome : Boolean)
    fun onFailure(msg : String)
    fun successInsertFavorite()
    fun successDeleteFavorite()
}