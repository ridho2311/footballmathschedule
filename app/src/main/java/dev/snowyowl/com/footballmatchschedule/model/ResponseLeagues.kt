package dev.snowyowl.com.footballmatchschedule.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

class ResponseLeagues(val leagues : ArrayList<Leagues>) {
    class DeserializeLeagues : ResponseDeserializable<ResponseLeagues>{
        override fun deserialize(content: String): ResponseLeagues? = Gson().fromJson(content, ResponseLeagues::class.java)
    }
}