package dev.snowyowl.com.footballmatchschedule.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.google.gson.Gson
import dev.snowyowl.com.footballmatchschedule.DetailScheduleActivity
import dev.snowyowl.com.footballmatchschedule.ankoUi.ItemScheduleUi
import dev.snowyowl.com.footballmatchschedule.model.EventSchedule
import dev.snowyowl.com.footballmatchschedule.utils.Converter
import org.jetbrains.anko.AnkoContext
import java.text.SimpleDateFormat
import java.util.*

class ScheduleAdapter(private var list : ArrayList<EventSchedule>) : RecyclerView.Adapter<ScheduleAdapter.ViewHolderSchedule>(), Filterable{
    private var listData : ArrayList<EventSchedule>? = null
    init {
        this.listData = list
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val listFilter = ArrayList<EventSchedule>()
                val strFilter = constraint.toString()
                if(strFilter == ""){
                    listFilter.addAll(list)
                }else{
                    for(data in list){
                        if(data.strAwayTeam.toLowerCase().contains(strFilter) || data.strHomeTeam.toLowerCase().contains(strFilter)){
                            listFilter.add(data)
                        }
                    }
                }
                val resultFilter = FilterResults()
                resultFilter.values = listFilter
                return resultFilter
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                listData = results?.values as ArrayList<EventSchedule>
                notifyDataSetChanged()
            }

        }
    }

    private var context : Context? = null
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderSchedule {
        this.context = p0.context
        return ViewHolderSchedule(ItemScheduleUi().createView(AnkoContext.create(p0.context, p0)))
    }

    override fun getItemCount(): Int = listData?.size!!

    override fun onBindViewHolder(p0: ViewHolderSchedule, p1: Int) {
        var dateForConvert = "${listData?.get(p1)?.dateEvent} ${listData?.get(p1)?.strTime}"
        dateForConvert = dateForConvert.removeRange(22,22)
        p0.txDate.text = Converter.getDate(dateForConvert)
        p0.txTime.text = Converter.getHours(dateForConvert)
        p0.txHomeTeam.text = listData?.get(p1)?.strHomeTeam
        p0.txHomeScore.text = listData?.get(p1)?.intHomeScore
        p0.txAwayTeam.text = listData?.get(p1)?.strAwayTeam
        p0.txAwayScore.text = listData?.get(p1)?.intAwayScore
        p0.itemView.setOnClickListener {
            context?.startActivity(Intent(context, DetailScheduleActivity::class.java)
                    .putExtra("data", Gson().toJson(listData?.get(p1))))
        }
    }

    class ViewHolderSchedule(itemView: View) : RecyclerView.ViewHolder(itemView){
        val txDate = itemView.findViewById<TextView>(ItemScheduleUi.txDate)!!
        val txTime = itemView.findViewById<TextView>(ItemScheduleUi.txTime)!!
        val txHomeTeam = itemView.findViewById<TextView>(ItemScheduleUi.txLeftTim)!!
        val txHomeScore= itemView.findViewById<TextView>(ItemScheduleUi.txLeftTimScore)!!
        val txAwayTeam = itemView.findViewById<TextView>(ItemScheduleUi.txRightTim)!!
        val txAwayScore= itemView.findViewById<TextView>(ItemScheduleUi.txRightTimScore)!!
    }
}