package dev.snowyowl.com.footballmatchschedule.utils

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.model.Player
import kotlinx.android.synthetic.main.activity_detail_player.*

class DetailPlayerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)
        initView()
    }

    private fun initView() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val dJson = intent.getStringExtra("data")
        val data = Gson().fromJson(dJson, Player::class.java)

        val img = findViewById<ImageView>(R.id.img_banner)
        Picasso.get().load(data.strThumb).into(img)
        tx_height.text = data.strHeight
        tx_weight.text = data.strWeight
        tx_info.text = data.strDescriptionEN
    }

    override fun getSupportParentActivityIntent(): Intent? {
        return super.getSupportParentActivityIntent()
    }
}
