package dev.snowyowl.com.footballmatchschedule.utils

import android.support.test.espresso.IdlingResource
import android.support.test.espresso.idling.CountingIdlingResource

class ExpressoIdlingResource {
    companion object {
        val mCountingIdlingRes = CountingIdlingResource("GLOBAL")

        fun increment(){
            mCountingIdlingRes.increment()
        }

        fun decrement(){
            mCountingIdlingRes.decrement()
        }
    }

    val idlingResource : IdlingResource
        get() = mCountingIdlingRes
}
