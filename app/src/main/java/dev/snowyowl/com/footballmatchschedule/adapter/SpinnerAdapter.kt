package dev.snowyowl.com.footballmatchschedule.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import dev.snowyowl.com.footballmatchschedule.model.Leagues
import org.jetbrains.anko.margin
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.padding
import org.jetbrains.anko.wrapContent

class SpinnerAdapter(val list : ArrayList<Leagues>) : BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = TextView(parent?.context)
        view.textSize = 16f
        view.text = list[position].strLeague
        val param = LinearLayout.LayoutParams(matchParent, wrapContent)
        param.margin = 4
        view.padding = 4
        view.layoutParams = param
        return view
    }

    override fun getItem(position: Int): Any = list[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = list.size
}