package dev.snowyowl.com.footballmatchschedule.ankoUi

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import dev.snowyowl.com.footballmatchschedule.R
import org.jetbrains.anko.*

class ItemTeam : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        linearLayout {
            lparams(width = matchParent, height = wrapContent){
                orientation = LinearLayout.HORIZONTAL
                margin = 8
            }
            imageView {
                id = R.id.img_badge
            }.lparams(width = 90, height = 90){
                marginEnd = 16
            }
            textView {
                id = R.id.tx_team_name
            }.lparams(width = wrapContent, height = matchParent){
                gravity = Gravity.CENTER_VERTICAL
            }
        }
    }
}