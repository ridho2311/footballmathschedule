package dev.snowyowl.com.footballmatchschedule.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.Spinner
import dev.snowyowl.com.footballmatchschedule.R
import dev.snowyowl.com.footballmatchschedule.adapter.ScheduleAdapter
import dev.snowyowl.com.footballmatchschedule.adapter.SpinnerAdapter
import dev.snowyowl.com.footballmatchschedule.ankoUi.ScheduleUi
import dev.snowyowl.com.footballmatchschedule.model.EventSchedule
import dev.snowyowl.com.footballmatchschedule.model.Leagues
import dev.snowyowl.com.footballmatchschedule.presenterView.SchedulePresenter
import dev.snowyowl.com.footballmatchschedule.presenterView.SchedulePresenterImp
import dev.snowyowl.com.footballmatchschedule.presenterView.ScheduleView
import dev.snowyowl.com.footballmatchschedule.utils.CoroutineContextProvider
import dev.snowyowl.com.footballmatchschedule.utils.ExpressoIdlingResource
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.toast

class ScheduleFragment : Fragment(), ScheduleView {

    private var presenterImp: SchedulePresenter? = null
    private var recyclerView: RecyclerView? = null
    private var rvAdapter: ScheduleAdapter? = null
    private var spinner: Spinner? = null
    private var spinnerAdapter : SpinnerAdapter? =null
    private var list: ArrayList<EventSchedule>? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var listLeagueComplete: ArrayList<Leagues>? = null
    private var idLeagues = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
            setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        if (arguments?.getString("key") == getString(R.string.favorites)) {
            list?.clear()
            recyclerView?.adapter?.notifyDataSetChanged()
            context?.let {
                ExpressoIdlingResource.increment()
                presenterImp?.getFavorite(it)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = ScheduleUi().createView(AnkoContext.create(this.context!!, this, false))
        presenterImp = context?.let { SchedulePresenterImp(this, CoroutineContextProvider()) }
        initView(view)
        ExpressoIdlingResource.increment()
        presenterImp?.getListLeagues()
        return view
    }

    override fun onSuccessSchedule(data: ArrayList<EventSchedule>) {
        swipeRefreshLayout?.isRefreshing = false
        list?.clear()
        recyclerView?.adapter?.notifyDataSetChanged()
        list?.addAll(data)
        recyclerView?.adapter?.notifyDataSetChanged()
        ExpressoIdlingResource.decrement()
    }

    override fun onSuccessLeagues(data: ArrayList<Leagues>) {
        if(data.isNotEmpty() && listLeagueComplete != null){
            listLeagueComplete?.addAll(data)
            spinnerAdapter?.notifyDataSetChanged()
        }
        ExpressoIdlingResource.decrement()
    }

    override fun onFailureNoSchedule(msg: String) {
        list?.clear()
        recyclerView?.adapter?.notifyDataSetChanged()
        toast(msg)
        ExpressoIdlingResource.decrement()
    }

    override fun onFailure(msg: String) {
        swipeRefreshLayout?.isRefreshing = false
        toast("Tidak tersedia")
        ExpressoIdlingResource.decrement()
    }

    override fun showLoading() {
        ExpressoIdlingResource.increment()
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
        ExpressoIdlingResource.decrement()
    }

    private fun getDataList() {
        ExpressoIdlingResource.increment()
        when {
            arguments?.getString("key") == getString(R.string.last_match) -> presenterImp?.getLastSchedule(idLeagues)
            arguments?.getString("key") == getString(R.string.next_match) -> presenterImp?.getNextSchedule(idLeagues)
            arguments?.getString("key") == getString(R.string.favorites) -> context?.let {
                spinner?.visibility = View.GONE
                presenterImp?.getFavorite(it) }
        }
    }

    private fun initView(view: View) {
        list = ArrayList()
        recyclerView = view.findViewById(ScheduleUi.recycleView)
        recyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        if(list != null){
            rvAdapter = ScheduleAdapter(list!!)
            recyclerView?.adapter = rvAdapter
        }
        swipeRefreshLayout = view.findViewById(R.id.swipe)
        swipeRefreshLayout?.setOnRefreshListener {
            swipeRefreshLayout?.isRefreshing = true
            list?.clear()
            recyclerView?.adapter?.notifyDataSetChanged()
            if(idLeagues != "")
                getDataList()
            else
                presenterImp?.getListLeagues()
        }
        spinner = view.findViewById(ScheduleUi.spinner)

        if(arguments?.getString("key") != getString(R.string.favorites)){
            listLeagueComplete = ArrayList()
            spinnerAdapter = SpinnerAdapter(listLeagueComplete!!)
            spinner?.adapter = spinnerAdapter
            spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    if(listLeagueComplete != null){
                        idLeagues = listLeagueComplete?.get(0)?.idLeague.toString()
                        spinner?.setSelection(0)
                    }else{
                        toast("Tidak tersedia")
                    }

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if(listLeagueComplete?.isNotEmpty()!!){
                        idLeagues = listLeagueComplete?.get(position)?.idLeague.toString()
                        ExpressoIdlingResource.increment()
                        getDataList()
                    }

                }
            }
        }else{
            spinner?.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        if(arguments?.getString("key") != getString(R.string.favorites)){
            inflater?.inflate(R.menu.option_menu, menu)
            val searchItem = menu?.findItem(R.id.menu_search)
            val searchView = searchItem?.actionView as SearchView
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    rvAdapter?.filter?.filter(p0)
                    return true
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    rvAdapter?.filter?.filter(p0)
                    return true
                }
            })
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

}
