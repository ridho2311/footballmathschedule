package dev.snowyowl.com.footballmatchschedule.presenterView

import android.content.Context
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import dev.snowyowl.com.footballmatchschedule.helper.*
import dev.snowyowl.com.footballmatchschedule.model.DbFavorite
import dev.snowyowl.com.footballmatchschedule.model.ResponseLeagues
import dev.snowyowl.com.footballmatchschedule.model.ResponseTeam
import dev.snowyowl.com.footballmatchschedule.model.Team
import dev.snowyowl.com.footballmatchschedule.utils.ApiAddress
import dev.snowyowl.com.footballmatchschedule.utils.CoroutineContextProvider
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.select

class TeamsPresenterImp(val view: TeamsView, private val contextProvider: CoroutineContextProvider) : TeamsPresenter {
    override fun getFavoriteTeam(context: Context) {
        view.showLoading()
        val l = context.database.use {
            select(tableNameTeam, idEvent, data).parseList(object : MapRowParser<ArrayList<DbFavorite>> {
                override fun parseRow(columns: Map<String, Any?>): ArrayList<DbFavorite> {
                    val list = ArrayList<DbFavorite>()
                    val id: String = columns[idEvent].toString()
                    val data: String = columns[data].toString()
                    list.add(DbFavorite(id, data))
                    return list
                }
            })
        }
        view.hideLoading()
        if (l.isNotEmpty()) {
            val favoriteList = ArrayList<Team>()
            for ((value) in l) {
                favoriteList.add(Gson().fromJson(value.data, Team::class.java))
            }
            view.onTeamLoaded(favoriteList)
        }else{
            view.onFailure("Tidak tersedia")
        }
    }

    override fun getListLeagues() {
        async(contextProvider.main){
            view.showLoading()
            ApiAddress.getAllLeague().httpGet().responseObject(ResponseLeagues.DeserializeLeagues()){
                _, _, result ->
                view.hideLoading()
                when (result) {
                    is Result.Success -> {
                        val (value) = result
                        if (value != null) {
                            if(value.leagues.isNotEmpty())
                                view.onSuccessLeagues(value.leagues)
                            else
                                view.onFailure("Tidak tersedia")
                        } else {
                            view.onFailure("Tidak tersedia")
                        }
                    }
                    is Result.Failure -> {
                        view.onFailure(result.error.message!!)
                    }
                }
            }
        }
    }

    override fun getTeams(idLeagues: String) {
        async(contextProvider.main){
            view.showLoading()
            ApiAddress.getAllTeamByLeagues(idLeagues.replace(" ", "%20")).httpGet().responseObject(ResponseTeam.DeserializeTeam()){
                request, response, result ->
                view.hideLoading()
                when (result) {
                    is Result.Success -> {
                        val (value) = result
                        if (value != null) {
                            if(value.teams != null)
                                view.onTeamLoaded(value.teams)
                            else
                                view.onFailure("Tidak tersedia")
                        } else {
                            view.onFailure("Tidak tersedia")
                        }
                    }
                    is Result.Failure -> {
                        view.onFailure(result.error.message!!)
                    }
                }
            }
        }

    }
}